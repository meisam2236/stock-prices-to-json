from bs4 import BeautifulSoup
from selenium  import webdriver
import csv

driver = webdriver.Chrome()
site = 'https://rahavard365.com/stock'
sites = []

driver.get(site)
soup = BeautifulSoup(driver.page_source,"lxml")

elems = driver.find_elements_by_class_name('symbol')
for elem in elems:
	sites.append(elem.get_attribute("href"))

driver.quit()

with open('sites.csv', mode='w') as csv_file:
	for line in sites:
		csv_file.write(line)
		csv_file.write('\n')