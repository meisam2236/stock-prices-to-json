from bs4 import BeautifulSoup
from selenium  import webdriver
import pandas as pd
import json

def replace_prices(text):
	return text.replace("٬","").replace("۰","0").replace("۱","1").replace("۲","2").replace("۳","3").replace("۴","4").replace("۵","5").replace("۶","6").replace("۷","7").replace("۸","8").replace("۹","9")

def replace_percentages(text):
	return text.replace("٪","").replace("٫",".").replace("٬",".").replace("۰","0").replace("۱","1").replace("۲","2").replace("۳","3").replace("۴","4").replace("۵","5").replace("۶","6").replace("۷","7").replace("۸","8").replace("۹","9")

driver = webdriver.Chrome()
sites = pd.read_csv('sites.csv').values
data = {}

for i in range(len(sites)):
	driver.get(sites[i][0])
	soup = BeautifulSoup(driver.page_source,"lxml")
	name = soup.select('.asset-symbol')[0].text.strip()
	final_percentage = soup.select('.symbolprices')[0].select("span")[4].text
	final_price = soup.select('.symbolprices')[0].select("span")[0].text
	
	data['stocks'] = []
	data['stocks'].append({
		'name': name,
		'close percentage': replace_percentages(final_percentage),
		'close price': replace_prices(final_price)
	})
		
with open('test.json', 'w') as outfile:
	json.dump(data, outfile)
	
with open('test.json') as json_file:
	data = json.load(json_file)
	for p in data['stocks']:
		print('name: ' + p['name'])
		print('close percentage: ' + p['close percentage'])
		print('close price: ' + p['close price'])
		print('')
		
driver.quit()
